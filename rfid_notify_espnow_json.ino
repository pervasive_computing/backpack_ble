#include <ArduinoJson.h>
#include <Arduino.h>
#include "RFID.h"
#include <list>
#include <esp_now.h>
#include <WiFi.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define SS_PIN 5 // RFID SDA-pin
#define RST_PIN 27 // RFID Reset-pin

#define SERVICE_UUID          "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID   "beb5483e-36e1-4688-b7f5-ea07361b26a8" // rfid tags
#define CHARACTERISTIC_UUID_2 "aeb5483e-36e1-4688-b7f5-ea07361b26a8" // device

#define backpack_id "1" // ID of this backpack

#define JSON_LD_BACKPACK_ID "id"
#define JSON_LD_BACKPACK_DESCRIPTION "description"
#define JSON_LD_ACTIONS "actions"
#define JSON_LD_PUSH_ITEMS "push_items"
#define JSON_LD_ACTION_DESCRIPTION "description"
#define JSON_LD_ITEMS "items"
#define JSON_LD_ITEM_ID "id"
#define JSON_LD_ITEM_BACKPACK_ID "backpack_id"

BLEServer* pServer = NULL;
BLECharacteristic* RFIDCharacteristic = NULL;
BLECharacteristic* DEVICECharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;

String old_output_RFID;
char old_output_DEVICE[256];
String smartphone_data;
String old_smartphone_data;
unsigned long previousMillisRFID = 0;
int pollingrate = 60; // standard pollingrate is 60 sec

// MAC adress of main backpack controller
uint8_t broadcastAddress[] = {0x78, 0xE3, 0x6D, 0x1B, 0x0B, 0xCC};

/* global vars */
RFID* rfid;

// ESP-NOW JSON
String jsondataOxiIn;
char jsondataOxiOut[256];
StaticJsonDocument<200> docdevice;
StaticJsonDocument<200> docSmartphone;

// formats MAC Address
void formatMacAddress(const uint8_t *macAddr, char *buffer, int maxLength) {
  snprintf(buffer, maxLength, "%02x:%02x:%02x:%02x:%02x:%02x", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
}

// function for receiving ESP-NOW data
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len) {
  char* buff = (char*) incomingData; // char buffer
  jsondataOxiIn = String(buff); // converting into STRING
  Serial.print("Recieved ");
  Serial.println(jsondataOxiIn); // JSON data will be printed

  DeserializationError error = deserializeJson(docdevice, jsondataOxiIn);

  if (!error) {
    DynamicJsonDocument devicedocout(1024);
    char macStr[18];
    formatMacAddress(mac, macStr, 18);
    devicedocout["id"] = macStr;
    devicedocout["name"] = docdevice["Device_Name"];

    JsonObject properties = devicedocout.createNestedObject("properties");
    JsonArray data = properties.createNestedArray("data");

    DynamicJsonDocument mode(64);
    mode["name"] = "mode";
    mode["value"] = docdevice["Mode"];
    data.add(mode);

    DynamicJsonDocument oxi(64);
    oxi["name"] = "Oxygen_Level";
    oxi["value"] = docdevice["Oxigen_Level"];
    data.add(oxi);

    DynamicJsonDocument heart(64);
    heart["name"] = "Heart_Rate";
    heart["value"] = docdevice["Heart_Rate"];
    data.add(heart);

    serializeJson(devicedocout, jsondataOxiOut); // serilizing JSON
    Serial.println(jsondataOxiOut);
  }

  else {
    Serial.print(F("deserializeJson() failed: ")); // just in case of an ERROR of ArduinoJSON
    Serial.println(error.f_str());
    return;
  }
}

// function for sending ESP-NOW data
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

/* receive text from smartphone */
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *RFIDCharacteristic) {
      std::string value = RFIDCharacteristic->getValue();

      if (value.length() > 0) {
        smartphone_data = "";
        for (int i = 0; i < value.length(); i++) {
          smartphone_data = smartphone_data + value[i];
        }
        Serial.println(smartphone_data);

        DeserializationError error = deserializeJson(docSmartphone, smartphone_data);

        if (!error) {
          const char* checktype = docSmartphone["device"];
          Serial.println(checktype);

          // if the data is meant for Oximeter, just forward it to Oximeter
          if (strcmp(checktype, "Oximeter") == 0) {
            esp_now_send(broadcastAddress, (uint8_t *) smartphone_data.c_str(), sizeof(smartphone_data) + 100); // sending "smartphone_data"; adjust last number (length)
          }

          // if the data is meant for Backpack, deserialize it further
          else if (strcmp(checktype, "Backpack") == 0) {
            pollingrate = docSmartphone["action"]["value"];
            Serial.println(pollingrate);
          }
        }

        else {
          Serial.print(F("deserializeJson() failed: ")); // just in case of an ERROR of ArduinoJSON
          Serial.println(error.f_str());
          return;
        }
      }
    }
};

void setup() {
  // Init Serial
  Serial.begin(115200);
  Serial.println("Waiting to read RFID tags...");

  // Set device as a Wi-Fi Station (ESP-NOW)
  WiFi.mode(WIFI_STA);

  // init ESP-NOW (2 way)
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // register peer
  esp_now_peer_info_t peerInfo;
  memset(&peerInfo, 0, sizeof(peerInfo));
  for (int ii = 0; ii < 6; ++ii )
  {
    peerInfo.peer_addr[ii] = (uint8_t) broadcastAddress[ii];
  }
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add peer");
    return;
  }

  // once ESP-NOW is successfully Init, we will register to send and receive data
  esp_now_register_recv_cb(OnDataRecv);
  esp_now_register_send_cb(OnDataSent);

  // init RFID controller
  rfid = new RFID(SS_PIN, RST_PIN);

  // Create the BLE Device
  BLEDevice::init("Emergency Backpack");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  RFIDCharacteristic = pService->createCharacteristic(
                         CHARACTERISTIC_UUID,
                         BLECharacteristic::PROPERTY_READ   |
                         BLECharacteristic::PROPERTY_WRITE  |
                         BLECharacteristic::PROPERTY_NOTIFY |
                         BLECharacteristic::PROPERTY_INDICATE
                       );

  // Create a sencond BLE Characteristic
  DEVICECharacteristic = pService->createCharacteristic(
                           CHARACTERISTIC_UUID_2,
                           BLECharacteristic::PROPERTY_READ   |
                           BLECharacteristic::PROPERTY_WRITE  |
                           BLECharacteristic::PROPERTY_NOTIFY |
                           BLECharacteristic::PROPERTY_INDICATE
                         );

  // Create a BLE Descriptor
  RFIDCharacteristic->addDescriptor(new BLE2902());
  DEVICECharacteristic->addDescriptor(new BLE2902());

  // Create a callback to receive text from smartphone
  RFIDCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0); // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
}

void loop() {
  unsigned long currentMillisRFID = millis(); // internal counter for RFID

  // notify changed value
  if (deviceConnected) {
    String rfid_id = rfid->getId(); // get ID of rfid tag
    Serial.println(rfid_id);

    // create a list to collect for every read rfid ID (in this case only one)
    std::list<String> detected_tags;
    detected_tags.push_back(rfid_id);

    DynamicJsonDocument doc(1024);
    doc[JSON_LD_BACKPACK_ID] = backpack_id;
    doc[JSON_LD_BACKPACK_DESCRIPTION] = "Backpack that contains a set of item used by paramedic for first aid";
    doc[JSON_LD_ACTIONS][JSON_LD_PUSH_ITEMS][JSON_LD_ACTION_DESCRIPTION] = "push items in backpack every " + std::to_string(pollingrate) + " seconds";

    JsonArray detected_tags_json_array = doc.createNestedArray(JSON_LD_ITEMS); // set detected_tags list
    for (const String tag_id : detected_tags) { // let's create an object for each rfid tag (item)
      if (!tag_id.isEmpty()) { // check if the item is actually detected or just an empty string
        DynamicJsonDocument item(64); // create a nested object (the item) like {"id": "99", "backpack_id": "7"}
        item[JSON_LD_ITEM_ID] = tag_id;
        item[JSON_LD_ITEM_BACKPACK_ID] = backpack_id;
        detected_tags_json_array.add(item); // add nested item to the array
      }
    }

    String outputRFID; // initialize string "output"; has to be here
    serializeJson(doc, outputRFID); // json file "doc" will be serialized to the string "output"

    RFIDCharacteristic->setValue(outputRFID.c_str());
    DEVICECharacteristic->setValue(jsondataOxiOut);

    if (outputRFID != old_output_RFID) { // only notify if data is different to old data
      old_output_RFID = outputRFID;
      RFIDCharacteristic->notify();
      previousMillisRFID = currentMillisRFID; // reset counting also here
    }
    else if (currentMillisRFID - previousMillisRFID >= pollingrate*1000) { // also notify every "pollingrate", no matter if there is a change
      previousMillisRFID = currentMillisRFID;
      RFIDCharacteristic->notify();
    }

    if (jsondataOxiOut != old_output_DEVICE) { // only send if data is different to old data
      std::copy(std::begin(jsondataOxiOut), std::end(jsondataOxiOut), std::begin(old_output_DEVICE));
      DEVICECharacteristic->notify();
    }

    delay(500); // bluetooth stack will go into congestion if too many packets are sent.
  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }

  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
